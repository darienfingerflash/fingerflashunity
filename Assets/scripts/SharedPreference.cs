﻿using UnityEngine;
using System.Collections;

using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using FullSerializer;


public class Level
{

	[SerializeField]
	public int levelNo;
	public string levelDataBase64String;
}

public class SharedPreference
{

	private static SharedPreference instance = null;


	public static SharedPreference GetInstance ()
	{
		if (instance == null) {
			instance = new SharedPreference ();
		}

		return instance;
	}
	//mycode
	//	public static byte[] SerializeToByteArray(this object obj)
	//	{
	//
	//		var bf = new BinaryFormatter();
	//		using (var ms = new MemoryStream())
	//		{
	//			bf.Serialize(ms, obj);
	//			return ms.ToArray();
	//		}
	//	}
	//end
	public bool IsRightHandPlayer ()
	{
		//assuming a right hand player if not set from settings
		if (PlayerPrefs.HasKey ("IsRightHandPlayer")) {
			string isRightHandPlayer = PlayerPrefs.GetString ("IsRightHandPlayer");
			return bool.Parse (isRightHandPlayer);
		} else {
			return true;
		}

	}

	public void  IsRightHandPlayer (bool isRightHand)
	{
		PlayerPrefs.SetString ("IsRightHandPlayer", isRightHand.ToString ());
	}

	/*
	public void DeletePatternForLevelFromPreference (int level)
	{

		int highestSavedLevel = GetHighestSavedLevel ();
		PlayerPrefs.DeleteKey (level.ToString ());
		highestSavedLevel -= 1;
		SetHighestSavedLevel (highestSavedLevel);
	}

	public void SavePatternForLevel (Transform circleContainer, int level)
	{

		ArrayList pattern = new ArrayList ();
		foreach (Transform circleTransform in circleContainer) {
			CircleObject circleObject = new CircleObject ();
			circleObject.posX = circleTransform.position.x;
			circleObject.posY = circleTransform.position.y;
			circleObject.posZ = circleTransform.position.z;

			pattern.Add (circleObject);

		}
		BinaryFormatter bf = new BinaryFormatter ();
		MemoryStream ms = new MemoryStream ();

		bf.Serialize (ms, pattern);
		byte[] bytearray = ms.ToArray ();
		string levelBase64String = System.Convert.ToBase64String (bytearray);
		PlayerPrefs.SetString (level.ToString (), levelBase64String);  //Save data to Preference

		int highestSavedLevel = GetHighestSavedLevel ();
		if (level > highestSavedLevel) {

			SetHighestSavedLevel (level);
		}

	}

	public void SetHighestSavedLevel (int level)
	{
		PlayerPrefs.SetInt ("highestSavedLevel", level);
	}

	public int GetHighestSavedLevel ()
	{
		return PlayerPrefs.GetInt ("highestSavedLevel");
	}

	public ArrayList GetPatternForLevel (int level)
	{
		string patternString = PlayerPrefs.GetString (level.ToString ());
		BinaryFormatter bf = new BinaryFormatter ();
		MemoryStream ms = new MemoryStream (System.Convert.FromBase64String (patternString));
		ArrayList pattern = bf.Deserialize (ms) as ArrayList;
		return pattern;

	}


	public  void WriteDataToFile ()
	{

		string path = null;
		#if UNITY_EDITOR
		path = "Assets/Resources/Files/Levels.json";
		#endif
		#if UNITY_STANDALONE
		// You cannot add a subfolder, at least it does not work for me
		path = "MyGame_Data/Resources/ItemInfo.json"
		#endif

		//ArrayList jsonList = new ArrayList ();
		string delimiterString = "";
		int highestLevel = GetHighestSavedLevel ();
//		for (int l = 1; l <= highestLevel; l++) {
//			Level level = new Level ();
//			level.levelNo = l;
//			level.levelDataBase64String = PlayerPrefs.GetString (l.ToString ());
//			string jsonString = JsonUtility.ToJson (level);
//			//jsonList.Add (jsonString);
//			delimiterString += jsonString;
//			delimiterString += '\n';
//
//		}

//		ArrayList patterns = new ArrayList ();
//		for (int l = 1; l <= highestLevel; l++) {
//			string levelB64 = GetPatternForLevel (l);
//			BinaryFormatter bf = new BinaryFormatter ();
//			MemoryStream ms = new MemoryStream (System.Convert.FromBase64String (base64PatternString));
//			ArrayList pattern = new ArrayList ();
//			try {
//				pattern = bf.Deserialize (ms) as ArrayList;
//
//			} catch (System.Exception e) {
//				Debug.Log ("Exception: " + e);
//			} finally {
//				patterns.Add (pattern);
//			}
//		}

		//delimiterString.Remove (delimiterString.Length - 3);
//		using (FileStream fs = new FileStream (path, FileMode.Create)) {
//			using (StreamWriter writer = new StreamWriter (fs)) {
//				writer.Write (delimiterString);
//			}
//		}
		File.WriteAllText (path, delimiterString);
		//string str = "A Test Sting";
	
		#if UNITY_EDITOR
		UnityEditor.AssetDatabase.Refresh ();
		#endif
	}

	public ArrayList ReadDataFromFile ()
	{
		string path = null;
		#if UNITY_EDITOR
		path = "Assets/Resources/Files/Levels.json";
		#endif
		#if UNITY_STANDALONE
		// You cannot add a subfolder, at least it does not work for me
		path = "MyGame_Data/Resources/ItemInfo.json"
		#endif

		string str = "";
//		using (FileStream fs = new FileStream (path, FileMode.Create)) {
//			using (StreamReader reader = new StreamReader (fs)) {
//				string line = "";
//				while ((line = reader.ReadLine ()) != null) {
//					str += line;
//				}
//			}
//		}
		str = File.ReadAllText (path);
		string[] jsonLevels = str.Split ('\n');

	
		ArrayList patterns = new ArrayList ();
		foreach (string jsonData in jsonLevels) {

			try {
				Level level = JsonUtility.FromJson<Level> (jsonData);
				string base64PatternString = level.levelDataBase64String;
				BinaryFormatter bf = new BinaryFormatter ();
				MemoryStream ms = new MemoryStream (System.Convert.FromBase64String (base64PatternString));
				ArrayList pattern = new ArrayList ();
				try {
					pattern = bf.Deserialize (ms) as ArrayList;

				} catch (System.Exception e) {
					Debug.Log ("Exception: " + e);
				} finally {
					patterns.Add (pattern);
				}

			} catch (System.Exception e) {
			
			} finally {
				
			}



		}
		return patterns;
	}

	*/
}