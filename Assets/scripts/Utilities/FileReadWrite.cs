﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

using System;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;

public class FileReadWrite : MonoBehaviour
{
	private const float SECONDS_BEFORE_TIMEOUT = 10;

	private const string URL = "";
	//Server URL from where to download the file

	private const string FILE_PATH = "Resources/Files/Levels.json";

	/*public void DownloadAndSave ()
	{
		StartCoroutine (DownloadCoroutine ());
	}*/




	public Dictionary<object, object> GetSavedData ()
	{
		// Use ReadContents() and do your MiniJSON magic here
		return null;    
	}

	/*private IEnumerator DownloadCoroutine ()
	{
		var requestHeaders = new Hashtable () {
			{ "Connection", "close" },
			{ "Accept", "application/json" }
		};

		using (var request = new WWW (URL, null, requestHeaders)) {
			float timeStarted = Time.realtimeSinceStartup;

			while (!request.isDone) {
				// Check if the download times out
				if (Time.realtimeSinceStartup - timeStarted > SECONDS_BEFORE_TIMEOUT) {
					Debug.Log ("Download timed out");
					yield break;
				}

				yield return null;
			}

			// Check for other errors
			if (request.error != null) {
				Debug.Log (request.error);

				yield break;
			}


			SaveContents (request.text);
		}
	}*/

	private string ReadContents ()
	{
		string ret;

		using (FileStream fs = new FileStream (FILE_PATH, FileMode.Open)) {
			BinaryReader fileReader = new BinaryReader (fs);

			ret = fileReader.ReadString ();

			fs.Close ();
		}

		return ret;
	}

	public static string ReadDataFromFile ()
	{

		//TextAsset textAsset = Resources.Load ("Files/Levels") as TextAsset;
		string path = Application.persistentDataPath + "/" + "pattern.json";
		Debug.Log ("PersistentPath:" + path);

		//string jsonString = textAsset.text;
		string jsonString = "";
		jsonString = File.ReadAllText (path);
		return jsonString;

//		TextAsset asset = Resources.Load ("Files/Levels") as TextAsset;
//		string jsonString;
//		jsonString = asset.text;
//		return jsonString;
	}

	public static void WriteDataToFile (string jsonString)
	{
		
//		StreamWriter writer = new StreamWriter ("Resources/Files/Levels.json"); // Does this work?
//		writer.WriteLine (jsonString);

//		string path = Application.dataPath + "/Resources/Files/Levels.json";
//		Debug.Log ("AssetPath:" + path);
//		File.WriteAllText (path, jsonString);
//		#if UNITY_EDITOR
//		UnityEditor.AssetDatabase.Refresh ();
//		#endif
//
//		#if UNITY_EDITOR
//		UnityEditor.AssetDatabase.Refresh ();
//		#endif
		ExportToExternalFile (jsonString);

	}



	private static  void ExportToExternalFile (string jsonString)
	{


		string fileName = Application.persistentDataPath + "/" + "pattern.json"; 
		Debug.Log ("FilePath:" + fileName);

		using (FileStream fs = new FileStream (fileName, FileMode.Create)) {
			using (StreamWriter writer = new StreamWriter (fs)) {
				writer.Write (jsonString);
			}
		}
		//SendMail (jsonString);
	}

	public static void ClearExternalFile ()
	{
		string fileName = Application.persistentDataPath + "/" + "pattern.json";
		using (FileStream fs = new FileStream (fileName, FileMode.Create)) {
			using (StreamWriter writer = new StreamWriter (fs)) {
				writer.Write ("");
			}
		}
	}

	public  static void SendMail (string jsonString)
	{
		Debug.Log ("Mail Begin");
		MailMessage mail = new MailMessage ();

		mail.From = new MailAddress ("deep.mobdev@gmail.com");
		mail.To.Add ("dep2k4u@gmail.com");
		mail.Subject = "FF Level Update";
		mail.Body = jsonString;

		SmtpClient smtpServer = new SmtpClient ("smtp.gmail.com");
		smtpServer.Port = 587;
		smtpServer.Credentials = new System.Net.NetworkCredential ("deep.mobdev@gmail.com", "Wonderful123mac") as ICredentialsByHost;
		smtpServer.EnableSsl = true;
		ServicePointManager.ServerCertificateValidationCallback = 
			delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) {
			return true;
		};
		smtpServer.Send (mail);
		Debug.Log ("Mail End");
	}

}