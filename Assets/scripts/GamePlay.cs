﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

//using SimpleJSON;

public class GamePlay : MonoBehaviour
{
	public Transform circlesContainer;
	public Sprite greenSprite;
	public Sprite blueSprite;

	public Text numberOfCirclesText;
	public Text numberOfTouchesText;
	public Text scoreText;
	public GameObject correctMatchPopUp;
	public GameObject wrongMatchPopUp;
	public GameObject levelsCompletePopUp;
	public Text wrongMatchPopUpText;
	public Text correctMatchPopUpText;
	public List<GameObject> circles = new List<GameObject> ();
	private ArrayList patterns = new ArrayList ();
	int nCircles = 0;
	int score = 0;
	int currentPatternIndexInShuffledPatternsList = 0;
	bool shouldDetectTouch = true;
	//Random random = new Random();//one persistent instance
	const string touchesExceedCirclesText = "Wrong match, No of touches exceeds No of Circles";
	const string touchedOutsideCircleText = "Wrong match, finger touches outside the circles";
	bool isRightHandPlayer;

	private int circleImageHeight = 100;
	//px
	private int circleImageWidthInPixel = 100;
	private float circleDiameterRequiredInInches = 0.472f;

	void Awake ()
	{
		Camera.main.orthographicSize = Convert.ToSingle (Screen.height / (200.0));
	}
	//	void CreateJsonFile(){
	//		SimpleJSON.JSONNode node = SimpleJSON.JSONNode.Parse(Resources.Load<TextAsset>
	//			("JSON/Test/test").text);
	//
	//		node["volume"].AsFloat = 0.5f;
	//
	//		File.WriteAllText(Environment.CurrentDirectory + "/Assets/Resources/JSON/Test/" + @"\audio.json", node.ToString());
	//	}
	// Use this for initialization
	void Start ()
	{
		
		isRightHandPlayer = SharedPreference.GetInstance ().IsRightHandPlayer ();
		//print ("IsRightHandPlayer:" + isRightHandPlayer);
		patterns = GetPatternsFromFile ();
		patterns = Helper.ShuffleArrayList (patterns, 1);
		//patterns.ra
		if (patterns.Count > 0) {
			ArrayList pattern = patterns [currentPatternIndexInShuffledPatternsList] as ArrayList;
			CreateCirclesForPattern (pattern);
		}
	
	
	}

	private ArrayList GetPatternsFromFile ()
	{
		string jsonString = FileReadWrite.ReadDataFromFile ();
		ArrayList patterns = new ArrayList ();
		try {
			patterns = JsonSerialization.Deserialize (typeof(ArrayList), jsonString) as ArrayList;
		} catch (Exception e) {
			Debug.Log ("Exception:" + e);
		}  
		return patterns;
	}

	public void PlayAgain ()
	{
		ResetGame ();
		levelsCompletePopUp.SetActive (false);
	}

	private void SetAllCircleColorBlue ()
	{
		foreach (Transform circle in circlesContainer) {
			circle.gameObject.GetComponent<SpriteRenderer> ().sprite = blueSprite;
		}
	}
	//Update is called once per frame
	void Update ()
	{
		
		if (shouldDetectTouch) {
			Touch[] touches = Input.touches;
			int touchCount = GetTouchCount (touches);
			SetNoOfToucheTextOnScreen (touchCount);
			if (touchCount > 0) {
				if (touchCount <= nCircles) {

					SetAllCircleColorBlue ();
					bool areAllTouchesInsideCircles = AreAllTouchesInsideCircles (touches);
					bool areNumberOfTouchesSameAsNumberOfCircles = AreNumberOfTouchesSameAsNumberOfCircles (touches);
					if (areAllTouchesInsideCircles) {
						if (areNumberOfTouchesSameAsNumberOfCircles) {
							shouldDetectTouch = false;
							UpdateScore ();
							RemoveAllCircles ();
							if (currentPatternIndexInShuffledPatternsList < patterns.Count - 1) {
								
								Invoke ("ShowNextPattern", 0.2f);
							} else {
								Debug.Log ("No more pattern to display");
								//levelsCompletePopUp.SetActive (true);
								//Shuffle the array Again
								ArrayList lastPattern = patterns [patterns.Count - 1] as ArrayList;
								patterns = Helper.ShuffleArrayList (patterns, 1);
								//ArrayList firstPattern = patterns [0];
								if (patterns.Count > 2) {
									ArrayList firstPattern = patterns [0] as ArrayList;
									while (firstPattern.Equals (lastPattern)) {
										patterns = Helper.ShuffleArrayList (patterns, 1);
										firstPattern = patterns [0] as ArrayList;
									}
								}
									

								currentPatternIndexInShuffledPatternsList = -1;
								Invoke ("ShowNextPattern", 0.2f); //It will set the currentPatternIndexInShuffledPatternsList = 0 first
//								try {
//									ArrayList pattern = patterns [currentPatternIndexInShuffledPatternsList] as ArrayList;
//									CreateCirclesForPattern (pattern);
//								} catch (Exception e) {
//									print ("Exception:" + e);
//								}

							}
					
							//ResetGame
						} else {
							Debug.Log ("Number of touches less than number of Circles");
						}
					} else {

						// Touched outside any circle
						//shouldDetectTouch = false;
						//ShowWrongAnswerPopUp (touchedOutsideCircleText);
					}
				} else {

					//Touch Count greater than number of touches
					//	shouldDetectTouch = false;
					//ShowWrongAnswerPopUp (touchesExceedCirclesText);

				}
			} else {
				SetAllCircleColorBlue ();
			}
		}
	
	}


	bool AreNumberOfTouchesSameAsNumberOfCircles (Touch[] touches)
	{
		if (touches.Length == circlesContainer.childCount) {
			return true;
		} else {
			return false;
		}
	}

	bool AreAllTouchesInsideCircles (Touch[] touches)
	{

		bool areAllTouchesInsideCircles = true;
		foreach (Touch touch in touches) {
			bool isTouchInsideCircle = IsTouchInsideAnyCircle (touch);
			if (!isTouchInsideCircle) {
				areAllTouchesInsideCircles = false;
				break;
			}
		}
		return areAllTouchesInsideCircles;
	}

	bool IsTouchInsideAnyCircle (Touch touch)
	{
		bool isTouchInsideCircle = false;
		RaycastHit2D hitInfo = Physics2D.Raycast (Camera.main.ScreenToWorldPoint (touch.position), Vector2.zero);
		// RaycastHit2D can be either true or null, but has an implicit conversion to bool, so we can use it like this
		if (hitInfo) {
			// Destroy( hitInfo.transform.gameObject );
			if (hitInfo.transform.gameObject != null) {
				
				isTouchInsideCircle = true;
				hitInfo.transform.gameObject.GetComponent<SpriteRenderer> ().sprite = greenSprite;
			}

			// Here you can check hitInfo to see which collider has been hit, and act appropriately.
		}
		if (!isTouchInsideCircle) {
			Debug.Log ("IsTouchInsideCircle: " + isTouchInsideCircle);
		}
     
		return isTouchInsideCircle;
	}

	//	void PlaceCirclesOnScreen ()
	//	{
	//	}
	//
	//	int GenerateRandomNumberRepresentingNoOfCirclesOnScreen ()
	//	{
	//
	//		int randomNo = (int)Random.Range (1, 5);
	//		return randomNo;
	//	}

	//	void CreateCirclesOnScreen (int nCircles)
	//	{
	//
	//
	//		string imageFileName;
	//		float xPos;
	//		float yPos;
	//
	//		for (int i = 0; i < nCircles; i++) {
	//			int randomNumber = Random.Range (0, circles.Count-1);
	//			xPos = Random.Range (-1, 1);
	//			yPos = Random.Range (-0.5f, 0.5f);
	//			GameObject prefab = circles [randomNumber];
	//			GameObject circle = GameObject.Instantiate (prefab);
	//			//circle.name = imageFileName;
	//			circle.transform.position = new Vector3 (xPos, yPos, circle.transform.position.z);
	//			circle.transform.parent = circlesContainer;
	//		}
	//
	//	}

	void SetNoOfCirclesTextOnScreen (int nCircles)
	{
		numberOfCirclesText.text = "No of Circles: " + nCircles;
	}

	void SetNoOfToucheTextOnScreen (int nTouches)
	{
		numberOfTouchesText.text = "No of Touches: " + nTouches;
	}

	public void TryAgainButtonClick ()
	{
		wrongMatchPopUp.SetActive (false);
		ResetGame ();

	}

	void ResetGame ()
	{
		
		shouldDetectTouch = true;
		score = 0;
		scoreText.text = "Score:" + score;

		// Shuffle the patterns List
		currentPatternIndexInShuffledPatternsList = 0;
		patterns = Helper.ShuffleArrayList (patterns, 1);
		ArrayList pattern = patterns [currentPatternIndexInShuffledPatternsList] as ArrayList;
		ShowPattern (pattern);

	}

	void ShowPattern (ArrayList pattern)
	{
		shouldDetectTouch = true;
		RemoveAllCircles ();
		//nCircles = GenerateRandomNumberRepresentingNoOfCirclesOnScreen();
		//SetNoOfCirclesTextOnScreen(nCircles);
		CreateCirclesForPattern (pattern);

		//CreateCirclesOnScreen(nCircles);
	}

	void ShowNextPattern ()
	{

		currentPatternIndexInShuffledPatternsList += 1;
		ArrayList pattern = patterns [currentPatternIndexInShuffledPatternsList] as ArrayList;
		ShowPattern (pattern);
	}

	void ShowWrongAnswerPopUp (string popUpText)
	{
		shouldDetectTouch = false;
		wrongMatchPopUp.SetActive (true);
		wrongMatchPopUpText.text = popUpText;
	}

	void ShowCorrectAnswerPopUp ()
	{
		correctMatchPopUp.SetActive (true);
	}

	void RemoveAllCircles ()
	{

		foreach (Transform circle in circlesContainer) {
			Destroy (circle.gameObject);
		}
	}

	void UpdateScore ()
	{
		score += 1;
		scoreText.text = "Score: " + score;
	}


	int GetTouchCount (Touch[] touches)
	{
		int fingerCount = 0;
		foreach (Touch touch in touches) {
			if (touch.phase != TouchPhase.Ended && touch.phase != TouchPhase.Canceled)
				fingerCount++;

		}
		return fingerCount;
	}


	public void CreateCirclesForPattern (ArrayList pattern)
	{

		//Debug.Log (((Vector3)pattern [0]).x);
		nCircles = pattern.Count;
		foreach (Vector3 circlePos in pattern) {

			// Circle Position saved in pattern is already in world coordinate
			float xPos = circlePos.x;
			if (!isRightHandPlayer)
				xPos = -1 * xPos;  //for left hand players
			
			GameObject prefab = circles [1];
			GameObject circle = getCircleObjectFromPrefab ();
			//Vector3 touchPos3D = Camera.main.ScreenToWorldPoint (new Vector3 (touchPosition.x, touchPosition.y, Camera.main.nearClipPlane));
			circle.transform.position = new Vector3 (xPos, circlePos.y, circlePos.z);
			circle.transform.parent = circlesContainer;
			circle.name = "Circle";
		}
		SetNoOfCirclesTextOnScreen (nCircles);
	}

	GameObject getCircleObjectFromPrefab ()
	{
		GameObject circlePrefab = circles [1];
		GameObject circle = GameObject.Instantiate (circlePrefab);
		float dpi = Screen.dpi;
		float circleDiameterInInches = circleImageWidthInPixel / dpi;      //circleImageWidth =100 always, dpi will change with screen
		float requiredScale = circleDiameterRequiredInInches / circleDiameterInInches;
		//	circle.transform.localScale.x = requiredScale;
		//	circle.transform.localScale.y = requiredScale;

		circle.transform.localScale = new Vector3 (requiredScale, requiredScale, circle.transform.localScale.z);
		return circle;
	}
	/*private ArrayList GetPatternsListFromPreference ()
	{
		SharedPreference sharedPref = SharedPreference.GetInstance ();
		ArrayList patternsList = new ArrayList ();
		if (PlayerPrefs.HasKey ("highestSavedLevel")) {
			int highestSavedLevel = sharedPref.GetHighestSavedLevel ();
			//	currentLevelNo = highestSavedLevel;

			for (int level = 1; level <= highestSavedLevel; level++) {

				ArrayList pattern = new ArrayList ();
				ArrayList circleObjects = sharedPref.GetPatternForLevel (level);
				foreach (CircleObject circleObject in circleObjects) {

					Vector3 circle = new Vector3 ();
					circle.x = circleObject.posX;
					circle.y =	circleObject.posY;
					circle.z = circleObject.posZ;	
					pattern.Add (circle);
				}
				patternsList.Add (pattern);
			}


		}
		return patternsList;
	}*/

	public void BackButtonClick ()
	{
		SceneManager.LoadScene ("MenuScene");
	}



}
