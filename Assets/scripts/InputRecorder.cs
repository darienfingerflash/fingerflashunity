﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;

public class InputRecorder : MonoBehaviour
{

	// Use this for initialization

	public Sprite blueSprite;
	public Sprite greenSprite;

	public GameObject circlePrefab;
	public Transform circlesContainer;
	public InputField levelNoInputField;
	public Text currentLevelText;
	private ArrayList savedPatterns = new ArrayList ();
	private int currentLevelNo;

	public GameObject twoButtonPanel;
	public GameObject threeButtonPanel;

	public GameObject savePatternBtn;
	public GameObject discardExistingPatternBtn;
	public GameObject discardNewPatternBtn;

	//ArrayList circleObjects = new ArrayList ();

	int count;
	private bool isEditingExistingPattern = false;
	private float circleRadius = 50;
	private float sideBarWidthInWorldCoordinates = 0.07f;
	// sideBarWidthinPixels/ScreenWidth
	private float sideBarWidthInPixels = 40;
	// ScreenWidth = 568
	//private float scaleFactorX;
	//private float scaleFactorY;
	public GameObject previousBtn;
	public GameObject nextBtn;
	private int circleImageHeight = 100;
	//px
	private int circleImageWidthInPixel = 100;
	private float circleDiameterRequiredInInches = 0.472f;

	void Awake ()
	{
		Camera.main.orthographicSize = Convert.ToSingle (Screen.height / (200.0));
		Debug.Log ("OrthoGraphics Size:" + Camera.main.orthographicSize);
		Canvas canvas = FindObjectOfType<Canvas> () as Canvas;
		float scaleFactor = canvas.scaleFactor;
		//circleRadius = circleRadius * scaleFactor;

		//		CanvasScaler canvasScaler = canvas.GetComponent<CanvasScaler> ();
		//		float scaleFactor = canvasScaler.scaleFactor;
		//		CanvasScaler.ScreenMatchMode match = canvasScaler.screenMatchMode;

		//	scaleFactorX = Convert.ToSingle (Screen.width / 568.0);
		//scaleFactorY = Convert.ToSingle (Screen.height / 320.0);
		//scaleFactorY = Convert.ToSingle (Screen.width / 568.0);

		sideBarWidthInPixels = 40 * scaleFactor;
		sideBarWidthInWorldCoordinates = sideBarWidthInPixels / Screen.width;
	}

	void Start ()
	{
		

		//scaleFactorY = 1;

		count = 0;
		savedPatterns = GetPatternsFromFile ();
		if (savedPatterns.Count > 0) {
			int highestSavedLevel = savedPatterns.Count;
			currentLevelNo = highestSavedLevel;
			CreateCirclesForPattern (savedPatterns [currentLevelNo - 1] as ArrayList);	    

		} else {
			//No data in file, 
			currentLevelNo = 0;
			savePatternBtn.SetActive (false);
			discardExistingPatternBtn.SetActive (false);
		}

		SetCurrentLevelText ();
		 
	}
		
	// Update is called once per frame
	void Update ()
	{
		// don't detect any touch if on Pattern Viewing Screen.

		if (currentLevelNo != 0) {
			SetAllCircleColorBlue ();
			Touch[] touches = Input.touches;
			foreach (Touch touch in touches) {
				if (threeButtonPanel.activeSelf) {
					ChangeCircleColorIfTouchInside (touch.position);
					//return;
				} else if (touch.phase == TouchPhase.Ended) {

					if (!DidTouchTopBar (touch.position)) {
						count += 1;
						Debug.Log ("Count:" + count);
						 
						bool didTouchDestroyCircle = DestroyCircleIfTouchInside (touch.position);
						if (!didTouchDestroyCircle)
							CreateCircleAtTouchPosition (touch.position);
						

					}

				}

			}

		}

	
	}

	GameObject getCircleObjectFromPrefab ()
	{
		GameObject circle = GameObject.Instantiate (circlePrefab);
		float dpi = Screen.dpi;
		float circleDiameterInInches = circleImageWidthInPixel / dpi;      //circleImageWidth =100 always, dpi will change with screen
		float requiredScale = circleDiameterRequiredInInches / circleDiameterInInches;
		//	circle.transform.localScale.x = requiredScale;
		//	circle.transform.localScale.y = requiredScale;

		circle.transform.localScale = new Vector3 (requiredScale, requiredScale, circle.transform.localScale.z);
		return circle;
	}

	void CreateCircleAtTouchPosition (Vector2 touchPosition)
	{
//		GameObject circle = GameObject.Instantiate (circlePrefab);
//		float dpi = Screen.dpi;
//		float circleDiameterInInches = circleImageWidthInPixel / dpi;      //circleImageWidth =100 always, dpi will change with screen
//		float requiredScale = circleDiameterRequiredInInches / circleDiameterInInches;
//		circle.transform.localScale = new Vector3 (requiredScale, requiredScale, circle.transform.localScale.z);

		GameObject circle = getCircleObjectFromPrefab ();

		float width = Screen.width;
		float height = Screen.height;

		float xPos = touchPosition.x;
		float yPos = touchPosition.y;

		if (xPos > (Screen.width - circleRadius)) {
			xPos = (Screen.width - circleRadius);
		} else if (xPos < (circleRadius + sideBarWidthInPixels)) {
			xPos = (circleRadius + sideBarWidthInPixels);
		}

		if (yPos > (Screen.height - circleRadius)) {
			yPos = (Screen.height - circleRadius);
		} else if (yPos < circleRadius) {
			yPos = circleRadius;
		}

		Vector3 touchPos3D = Camera.main.ScreenToWorldPoint (new Vector3 (xPos, yPos, Camera.main.nearClipPlane));
		circle.transform.position = new Vector3 (touchPos3D.x, touchPos3D.y, circle.transform.position.z);
		circle.transform.parent = circlesContainer;
		circle.name = "Circle";
	}

	// If touch is inside the circle, destroy it
	bool DestroyCircleIfTouchInside (Vector3 touchPosition)
	{
		bool isTouchedInside = false;
		RaycastHit2D hitInfo = Physics2D.Raycast (Camera.main.ScreenToWorldPoint (touchPosition), Vector2.zero);
		// RaycastHit2D can be either true or null, but has an implicit conversion to bool, so we can use it like this
		if (hitInfo) {
			if ((hitInfo.transform.gameObject != null) && (hitInfo.transform.gameObject.name == "Circle")) {

				hitInfo.transform.gameObject.GetComponent<SpriteRenderer> ().sprite = greenSprite;
				Destroy (hitInfo.transform.gameObject);
				isTouchedInside = true;
			}
			
		}
		Debug.Log ("Is Touched Inside:" + isTouchedInside);
		return isTouchedInside;
	}

	bool ChangeCircleColorIfTouchInside (Vector3 touchPosition)
	{
		bool isTouchedInside = false;
		RaycastHit2D hitInfo = Physics2D.Raycast (Camera.main.ScreenToWorldPoint (touchPosition), Vector2.zero);
		// RaycastHit2D can be either true or null, but has an implicit conversion to bool, so we can use it like this
		if (hitInfo) {
			if ((hitInfo.transform.gameObject != null) && (hitInfo.transform.gameObject.name == "Circle")) {

				hitInfo.transform.gameObject.GetComponent<SpriteRenderer> ().sprite = greenSprite;
				//hitInfo.transform.gameObject.GetComponent<SpriteRenderer> ().sprite = greenSprite;
				isTouchedInside = true;
			}

		}
		Debug.Log ("Is Touched Inside:" + isTouchedInside);
		return isTouchedInside;
	}


	public void NextPattern ()
	{
		if (currentLevelNo < savedPatterns.Count) {
			ClearScreen ();
			currentLevelNo += 1;
			SetCurrentLevelText ();
			ArrayList pattern = GetPatternForLevel (currentLevelNo);
			CreateCirclesForPattern (pattern);
		
		}
	}

	public void PreviousPattern ()
	{

		if (currentLevelNo > 1) {
			currentLevelNo -= 1;
			SetCurrentLevelText ();
			ClearScreen ();
			ArrayList pattern = GetPatternForLevel (currentLevelNo);
			CreateCirclesForPattern (pattern);

		}
			
	}



	public void GoButtonClick ()
	{

		int levelNo = 0;

		if (Int32.TryParse (levelNoInputField.text, out levelNo)) {
			// you know that the parsing attempt
			// was successful
			if (DoesPatternExistForLevel (levelNo)) {
				ClearScreen ();
				ArrayList pattern = GetPatternForLevel (levelNo);  //get pattern for level
				CreateCirclesForPattern (pattern);
			}
		} else {

		}

	}

	public void ClearScreen ()
	{

		foreach (Transform circle in circlesContainer) {
			Destroy (circle.gameObject);
		}
	}

	public bool DoesPatternExistForLevel (int levelNo)
	{
		if ((levelNo > 0) && (levelNo <= savedPatterns.Count))
			return true;
		else
			return false;
	}



	private ArrayList GetPatternForLevel (int levelNo)
	{
		ArrayList pattern = savedPatterns [levelNo - 1] as ArrayList;
		return  pattern; //level 1 starts from index 0
	}

	public void CreateCirclesForPattern (ArrayList pattern)
	{
	
		//Debug.Log (((Vector3)pattern [0]).x);

		foreach (Vector3 circlePos in pattern) {

		
			//	GameObject circle = GameObject.Instantiate (circlePrefab);
			GameObject circle = getCircleObjectFromPrefab ();
			circle.transform.position = new Vector3 (circlePos.x, circlePos.y, circlePos.z);
			circle.transform.parent = circlesContainer;
			circle.name = "Circle";
		}

	}

	private void SetCurrentLevelText ()
	{
		int highestSavedLevel = savedPatterns.Count;
		//int highestSavedLevel = SharedPreference.GetInstance ().GetHighestSavedLevel ();
		int fromValue = highestSavedLevel;
		if (currentLevelNo > highestSavedLevel)
			fromValue = currentLevelNo;
		currentLevelText.text = currentLevelNo + " / " + fromValue; 
	}

	private bool DidTouchTopBar (Vector3 touchPos)
	{
		bool isTouchedOnTopBar = false;
		if (touchPos.x < Screen.width * sideBarWidthInWorldCoordinates)
			isTouchedOnTopBar = true;
		else
			isTouchedOnTopBar = false;
		Debug.Log ("Is Touched Inside:" + isTouchedOnTopBar);
		return isTouchedOnTopBar;
	}

	// Two Button Panel Methods
	public void SaveNewPattern ()
	{
		if (circlesContainer.childCount > 0) {

			ArrayList pattern = new ArrayList ();
			foreach (Transform transform in circlesContainer) {
				pattern.Add (transform.position);
			}
			if (isEditingExistingPattern) {
				savedPatterns [currentLevelNo - 1] = pattern;
				isEditingExistingPattern = false;
				discardNewPatternBtn.SetActive (true);
			} else {
				savedPatterns.Add (pattern);   //New Pattern
			}

			twoButtonPanel.SetActive (false);
			threeButtonPanel.SetActive (true);
			savePatternBtn.SetActive (true);
			discardExistingPatternBtn.SetActive (true);
			//currentPatternNo = savedPatterns.Count + 1;
			SetCurrentLevelText ();

			//SharedPreference sharedPref = SharedPreference.GetInstance ();
			//sharedPref.SavePatternForLevel (circlesContainer, currentLevelNo);

		} else {
			Debug.Log ("No circle on screen, cant save the Empty pattern");
		}


	}

	public void DiscardNewPattern ()
	{

		// Pattern could be or couln't be saved Saved in Preference

		currentLevelNo = savedPatterns.Count;
		//SharedPreference.GetInstance().DeletePatternForLevelFromPreference(currentLevelNo);  //Pattern not saved in preference Yet
		ClearScreen ();
		twoButtonPanel.SetActive (false);
		threeButtonPanel.SetActive (true);
		SetCurrentLevelText ();

		if (currentLevelNo >= 1) {
			ArrayList pattern = GetPatternForLevel (currentLevelNo);
			CreateCirclesForPattern (pattern);
		}

	}


	// Three Button Panel Methods

	public void NewButtonClick ()
	{

		currentLevelNo = savedPatterns.Count + 1;
		twoButtonPanel.SetActive (true);
		threeButtonPanel.SetActive (false);
		//previousBtn.setActive (false);
		previousBtn.SetActive (false);
		nextBtn.SetActive (false);

		ClearScreen ();
		SetCurrentLevelText ();
	}

	public void SaveExistingPattern ()
	{

//		if (savedPatterns.Count == 0) {
//			return;
//		}
//		if (circlesContainer.childCount > 0) {
//			ArrayList pattern = new ArrayList ();
//			foreach (Transform circle in circlesContainer) {
//				pattern.Add (circle.position);
//			}
//			savedPatterns [currentLevelNo - 1] = pattern;
//
//		} else {
//			Debug.Log ("No circle on screen, cant save the Empty pattern");
//		}
//			
//		SharedPreference sharedPref = SharedPreference.GetInstance ();
//		sharedPref.SavePatternForLevel (circlesContainer, currentLevelNo);
	}

	public void DiscardExistingPattern ()
	{

		// Pattern is already in Preference, delete it from there
		//SharedPreference.GetInstance ().DeletePatternForLevelFromPreference (currentLevelNo);

		if (savedPatterns.Count == 0) {
			ClearScreen ();
			return;
		}
		if (savedPatterns.Count == 1) {

			currentLevelNo = 0;
			savedPatterns.RemoveAt (0);
			ClearScreen ();
			SetCurrentLevelText ();
			return;
		}
		//  if 2 or more entries, following code will run

		savedPatterns.RemoveAt (currentLevelNo - 1);
		currentLevelNo -= 1;
		ClearScreen ();
		SetCurrentLevelText ();
		if (currentLevelNo > 0) {
			ArrayList pattern = GetPatternForLevel (currentLevelNo);
			CreateCirclesForPattern (pattern);
		}


				
		// Find Which Entry to Delete From Array
		// Delete pattern from savedPatterns Array
		// Update Level No Label
		// Clear Screen
		// Show Previous or Next

		return;
	}

	public void BackButtonClick ()
	{
		SceneManager.LoadScene ("MenuScene");
	}

	public void EditButtonClick ()
	{

		isEditingExistingPattern = true;
		discardNewPatternBtn.SetActive (false);
		threeButtonPanel.SetActive (false);
		twoButtonPanel.SetActive (true);

		ClearScreen ();

		try {
			if (currentLevelNo > 0) {
				ArrayList pattern = GetPatternForLevel (currentLevelNo);
				CreateCirclesForPattern (pattern);
			}
		} catch (Exception e) {
			//currentLevelNo -= 1;  // exception will occur when backbtn pressed without 
		}
		SetCurrentLevelText ();

	}

	private void SetAllCircleColorBlue ()
	{
		foreach (Transform circle in circlesContainer) {
			circle.gameObject.GetComponent<SpriteRenderer> ().sprite = blueSprite;
		}
	}

	public void UpdatePatternFile ()
	{
		string jsonString = JsonSerialization.Serialize (typeof(ArrayList), savedPatterns);
		FileReadWrite.WriteDataToFile (jsonString);			
	}

	private ArrayList GetPatternsFromFile ()
	{
		string jsonString = FileReadWrite.ReadDataFromFile ();
		Debug.Log ("JSON String:" + jsonString);
		ArrayList patterns = new ArrayList ();
		try {
			patterns = JsonSerialization.Deserialize (typeof(ArrayList), jsonString) as ArrayList;
		} catch (Exception e) {
			Debug.Log ("Exception:" + e);
		}  
		return patterns;
	}

	public void BackFromEditScreenClick ()
	{
		int savedPatternsCount = savedPatterns.Count;
		if (isEditingExistingPattern) {

		} else {
			if (savedPatternsCount < currentLevelNo) {  // Back Button pressed without Saving
				currentLevelNo -= 1;
			}
		}
		ClearScreen ();
		SetCurrentLevelText ();
		threeButtonPanel.SetActive (true);
		twoButtonPanel.SetActive (false);
		nextBtn.SetActive (true);
		previousBtn.SetActive (true);

		if (currentLevelNo > 0) {
			try {
				ArrayList pattern = GetPatternForLevel (currentLevelNo);
				CreateCirclesForPattern (pattern);
			} catch (Exception e) {
				print (e);
			}
		}
	
	}

	public void ShiftPatternLeft ()
	{
		if (currentLevelNo > 1) {
			ArrayList currentPattern = GetPatternForLevel (currentLevelNo);
			ArrayList previousPattern = GetPatternForLevel (currentLevelNo - 1);
			//swap current with Previous
			savedPatterns [currentLevelNo - 1] = previousPattern;
			savedPatterns [currentLevelNo - 2] = currentPattern;


			ClearScreen ();
			currentLevelNo -= 1;
			SetCurrentLevelText ();
			try {
				ArrayList pattern = GetPatternForLevel (currentLevelNo);
				CreateCirclesForPattern (pattern);
			} catch (Exception e) {
				print (e);
			}
		
		}

	}

	public void ShiftPatternRight ()
	{
		if (currentLevelNo < savedPatterns.Count) {
			ArrayList currentPattern = GetPatternForLevel (currentLevelNo);
			ArrayList nextPattern = GetPatternForLevel (currentLevelNo + 1);
			savedPatterns [currentLevelNo - 1] = nextPattern;
			savedPatterns [currentLevelNo] = currentPattern;

			ClearScreen ();
			currentLevelNo += 1;
			SetCurrentLevelText ();
			try {
				ArrayList pattern = GetPatternForLevel (currentLevelNo);
				CreateCirclesForPattern (pattern);
			} catch (Exception e) {
				print (e);
			}
		}
	}


	public void GridButtonClick ()
	{
		SceneManager.LoadScene ("GridScene");
	}

}
