﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Settings : MonoBehaviour
{
	public Toggle leftHandToggle;
	public Toggle rightHandToggle;
	// Use this for initialization
	void Start ()
	{
		bool isRightHandPlayer = SharedPreference.GetInstance ().IsRightHandPlayer ();
		if (isRightHandPlayer) {
			leftHandToggle.isOn = false;
			rightHandToggle.isOn = true;
		} else {
			leftHandToggle.isOn = true;
			rightHandToggle.isOn = false;
		}

	}

	public void BackBtnClick ()
	{
		SceneManager.LoadScene ("MenuScene");
	}

	public void RightHandToggleStateChanged ()
	{
		if (rightHandToggle.isOn) {
			leftHandToggle.isOn = false;
		} else {
			leftHandToggle.isOn = true;
		}
		SharedPreference.GetInstance ().IsRightHandPlayer (rightHandToggle.isOn);
	}

	public void LeftHandToggleStateChanged ()
	{
		if (leftHandToggle.isOn) {
			rightHandToggle.isOn = false;
		} else {
			rightHandToggle.isOn = true;
		}
		SharedPreference.GetInstance ().IsRightHandPlayer (rightHandToggle.isOn);
	}


}
