﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

//Single Player
//Practice Mode
//Solo Race
//Solo Gauntlet

//Multiplayer
//Duel
//Gauntlet
//Match
//Tournament

public class Menu : MonoBehaviour
{

	// Use this for initialization
	void Start ()
	{
		//Debug.Log ("Path1: " + Application.persistentDataPath);
		//Debug.Log ("Path2: " + Application.dataPath + "/Resources/Files/Levels.json");

		//FileReadWrite.SendMail ();
		//PlayerPrefs.DeleteAll ();
		//FileReadWrite.WriteDataToFile ("");
		//SharedPreference.GetInstance ().WriteDataToFile ();
		//ArrayList patterns = SharedPreference.GetInstance ().ReadDataFromFile ();
		//Debug.Log ("Patterns:" + patterns);
		//FileReadWrite.ClearExternalFile ();
	
		//FileReadWrite.ClearExternalFile ();
	}
	// Update is called once per frame
	void Update ()
	{
	
	}

	public void menuButtonClick ()
	{

		SceneManager.LoadScene ("GamePlayScene");
	}


	public void patternButtonClick ()
	{
		SceneManager.LoadScene ("InputRecorderScene");
	}


	public void SettingsBtnClick ()
	{
		SceneManager.LoadScene ("SettingsScene");
	}

}
