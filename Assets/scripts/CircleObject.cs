﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class CircleObject {

	public string name;
	public int id;
	public bool dontLoad;
	public float posX;
	public float posY;
	public float posZ;

}
