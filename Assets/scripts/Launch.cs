﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Launch : MonoBehaviour {
	const int launchTime = 3;

	// Use this for initialization
	void Start () {

		Invoke ("LaunchScene", launchTime);
	
	}
	
	void LaunchScene(){
		SceneManager.LoadScene ("MenuScene");
	}
}
